# Code to reproduce results presented in the paper "The emergence of the fractal bronchial tree"

Available are following modules:

 - "image_preprocessing" - module for image preprocessing for segmentation and  skeletonization
 - "segmentation" - module for segmentation of lung images. Uses apoc to generate trainingsdata and a UNET to segment the lung images
 - "generate_lung_skeletons" - module for generating skeletons to train the "Skeleplex" network
 - "skeleplex" - module to train the skeleplex network and predict skeletons from segmentation images
 - "break_detection" - module to generate a graph from skeleplex output and detect breaks in the skeleton
 - "napari_skeleton_curator" - module to curate the skeletons and generate the final measurement tables. Utilizes napari.


 To install napari visit:

 https://napari.org/dev/tutorials/fundamentals/installation.html



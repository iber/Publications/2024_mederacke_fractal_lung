import os

import h5py
from monai.inferers import sliding_window_inference
import numpy as np
import torch

from morphospaces.networks.unet import SemanticSegmentationUnet
from morphospaces.networks.unetr import SemanticUnetr

from monai.losses import DiceCELoss

if __name__ == "__main__":
    # paths to data and model checkpoint
    file_name = '/path/to/image.h5'
    image_key = "image"
    checkpoint_path = "./checkpoints/checkpoints/vit-best.ckpt"

    output_path="./predict/"
    sw_batch_size = 2
    overlap = 0.4
    patch_shape = (64, 64, 64)

    segmentation_model = SemanticSegmentationUnet.load_from_checkpoint(checkpoint_path=checkpoint_path,
            in_channels=1,
            out_channels=2,
            image_shape=patch_shape,
            feature_size=16,
            hidden_size=768,
            mlp_dim=3072,
            n_heads=12,
            positional_embedding="perceptron",
            feature_normalization="instance",
            residual_block=True,
            convolutional_block=True,
            dropout_rate=0.0,
            # loss_function= SoftDiceclDiceLoss(iter_ = 20, alpha = 0.05),
            loss_function= DiceCELoss(to_onehot_y=True, softmax=True),
            image_key= 'image', 
            label_key= 'label',
        )
    





    
    # load the image
    with h5py.File(file_name, "r") as f:
        raw_image = f[image_key][:]

    raw_image = torch.from_numpy(raw_image)
    raw_image.shape
  
    # )
    # skeleton_model = SemanticSegmentationUnet.load_from_checkpoint(skeleton_checkpoint_path, out_channels=2)
    



    # checkpoint = torch.load('/home/mmederacke/lung_fractal/images/E15.5_mesospim/UNETR/checkpoints/vit-best.ckpt')
    # # skeleton_model = SemanticSegmentationUnet(
    # #         spatial_dims=3,
    # #         strides=(1,1,1,1),
    # #         image_key="image",
    # #         label_key="label",
    # #         in_channels=1,
    # #         out_channels=2,
    # #         learning_rate=0.0001,
    # #         # roi_size=(24,24,24),
    # #         roi_size=(96,96,96)
    # #         # roi_size= (64,64)
            
    # #         # roi_size = (-1,-1,-1)
    # #         )

    # skeleton_model = SemanticUnetr(
    #         in_channels=1,
    #         out_channels=2,
    #         image_shape=patch_shape,
    #         feature_size=16,
    #         hidden_size=768,
    #         mlp_dim=3072,
    #         n_heads=12,
    #         positional_embedding="perceptron",
    #         feature_normalization="instance",
    #         residual_block=True,
    #         convolutional_block=True,
    #         dropout_rate=0.0,
    #         # loss_function= SoftDiceclDiceLoss(iter_ = 20, alpha = 0.05),
    #         loss_function= DiceCELoss(to_onehot_y=True, softmax=True),
    #         image_key= 'image', 
    #         label_key= 'label',
    #     )

    # model_dict = skeleton_model.state_dict()

    # # Filter out unnecessary keys from the checkpoint
    # checkpoint = {k: v for k, v in checkpoint['state_dict'].items() if k in model_dict}

    # # Load the filtered state_dict
    # skeleton_model.load_state_dict(checkpoint, strict=False)
    # skeleton_model = torch.load(skeleton_checkpoint_path)
    # cp = torch.load(skeleton_checkpoint_path)

    # unet.load_state_dict(cp['state_dict'])
    # skeleton_model = unet

    segmentation_model.eval()
    segmentation_model.to(torch.device("cuda"),dtype=torch.float32)

    # make the prediction
    # you may have to apply a softmax across the channel dimension
    with torch.no_grad():
        semantic_prediction = sliding_window_inference(
            inputs=raw_image,
            # roi_size=(128, 128, 128),
            roi_size = (96,96,96),
            sw_batch_size=sw_batch_size,
            sw_device="cuda",
            device="cpu",
            overlap=overlap,
            mode="gaussian",
            predictor=segmentation_model
        )

    # save the image
    file_base = os.path.basename(file_name)
    output_file_name = file_base.replace(".h5", "_prediction.h5")
    with h5py.File(output_path+output_file_name, "w") as f_out:
        f_out.create_dataset(
            "prediction",
            data=semantic_prediction,
            compression="gzip"
        )


import pytorch_lightning as pl
import numpy as np
from monai.data import DataLoader
from monai.transforms import Compose, DivisiblePadd, SpatialPadd
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers import TensorBoardLogger

from morphospaces.datasets import StandardHDF5Dataset
from morphospaces.networks.unetr import SemanticUnetr
from morphospaces.transforms.image import ExpandDimsd, ImageAsFloat32
from morphospaces.transforms.label import LabelToBoundaryd
from monai.losses.cldice import SoftDiceclDiceLoss
from morphospaces.networks.unet import SemanticSegmentationUnet
from monai.transforms import (
    AsDiscrete,
    EnsureChannelFirstd,
    Compose,
    CropForegroundd,
    LoadImaged,
    Orientationd,
    RandFlipd,
    RandCropByPosNegLabeld,
    RandShiftIntensityd,
    ScaleIntensityRanged,
    Spacingd,
    RandRotate90d,
    RandGaussianNoised,
    RandGaussianSmoothd,
    RandAffined,
    RandRotated,
    Zoomd,

)
from typing import Dict, List, Union
import wandb
from pytorch_lightning.loggers import WandbLogger

class LabelToBackgroundd:
    """Set a label value to background (0).

    This is intended to be used with datasets where the
    data are loaded as a dictionary.

    Parameters
    ----------
    keys : Union[str, List[str]]
        The keys in the dataset to apply the transform to.
    """

    def __init__(self, keys: Union[str, List[str]], label_value: int, background_value: int=0):
        if isinstance(keys, str):
            keys = [keys]
        self.keys: List[str] = keys
        self.label_value = label_value
        self.background_value = background_value

    def __call__(
        self, data_item: Dict[str, np.ndarray]
    ) -> Dict[str, np.ndarray]:
        for key in self.keys:
            image = data_item[key]
            image[image == self.label_value] = self.background_value
            data_item.update({key: image})

        return data_item


if __name__ == "__main__":
    batch_size = 1
    patch_shape = (70, 70, 70)
    # patch_shape = (128,128,128)
    # patch_shape = (50,50,50)
    lr = 1e-4
    logdir_path = "./checkpoints_unet_dice_loss"

    # set the RNG seeds
    pl.seed_everything(42, workers=True)
    


    train_transform = Compose(
        [   
            # LabelToBackgroundd(keys=["label"], label_value=0, background_value=2),

            ImageAsFloat32(keys=["image", "label"]),
            # ImageAsFloat32(keys=["image"]), 
            # SpatialPadd(keys=["image", "label"], spatial_size=[96, 96, 96]),
            ExpandDimsd(keys=["image", "label"]),
            DivisiblePadd(keys=["image", "label"], k=16),
            # Spacingd(keys=["image", "label"], pixdim=(0.5, 0.5, 0.5), mode=("bilinear", "nearest")),
            RandAffined(keys = ["image","label"], prob = 0.1, translate_range = 10),
            RandRotated(keys = ["image","label"], prob = 0.1, range_x = 10, range_y = 10, range_z = 10),
            RandGaussianNoised(keys = ["image"], prob = 0.1, std = 0.05),
            RandGaussianSmoothd(keys = ["image"], prob = 0.1, sigma_x=(0.3, 1.0), sigma_y=(0.3, 1.0), sigma_z=(0.3, 1.0)),
            # Zoomd(keys = ["image", "label"], zoom = 1.3, prob = 0.2, min_zoom = 0.9, max_zoom = 1.1),
            RandFlipd(keys = ["image", "label"], prob = 0.1, spatial_axis = (0, 1, 2)),
        ]  
        )
    train_ds = StandardHDF5Dataset.from_glob_pattern(
            glob_pattern="./train/*.h5",
            transform=train_transform,
            patch_shape=patch_shape,
            # stride_shape=patch_shape,
            patch_filter_ignore_index=(0,),
            patch_threshold=0.35,
            patch_slack_acceptance=0.01,
            dataset_keys= ['image', 'label'],

        )
    print(train_ds.datasets[0].data['image'].shape)
    train_loader = DataLoader(
        train_ds, batch_size=batch_size, shuffle=True, num_workers=4
    )

    val_transform = Compose(
        [   
            # LabelToBackgroundd(keys=["label"], label_value=0, background_value=2),

            ImageAsFloat32(keys=["image", "label"]),

            # ImageAsFloat32(keys=["image"]),

            ExpandDimsd(keys=["image", "label"]),
            # LabelToBoundaryd(label_key="label"),
            # SpatialPadd(keys=["image", "label"], spatial_size=[96, 96, 96]),
            # DivisiblePadd(keys=["image", "label"], k=16),
        ]
    )

    val_ds = StandardHDF5Dataset.from_glob_pattern(
        glob_pattern="./val/*.h5",
        transform=val_transform,
        patch_shape=patch_shape,
        # stride_shape=patch_shape,
        patch_filter_ignore_index=(0,),
        patch_threshold=0.35,
        patch_slack_acceptance=0.01,
        dataset_keys= ['image', 'label'],


    )
    val_loader = DataLoader(
        val_ds, batch_size=batch_size, shuffle=False, num_workers=4
    )

    #use normal UNET
    unet = SemanticSegmentationUnet(
        spatial_dims=3,
        strides=(1,1,1,1),
        image_key="image",
        label_key="label",
        in_channels=1,
        out_channels=2,
        learning_rate=lr,
        # roi_size=(24,24,24),
        roi_size=patch_shape
        # roi_size= (64,64)
        
        # roi_size = (-1,-1,-1)
    )
    # unet.loss_function = SoftDiceclDiceLoss(iter_ = 25, alpha = 0.01)
    

    # make the model if you want to use UNETR
    # unet = SemanticUnetr(
    #         in_channels=1,
    #         out_channels=2,
    #         image_shape=patch_shape,
    #         feature_size=16,
    #         hidden_size=768,
    #         mlp_dim=3072,
    #         n_heads=12,
    #         positional_embedding="perceptron",
    #         feature_normalization="instance",
    #         residual_block=True,
    #         convolutional_block=True,
    #         dropout_rate=0.0,
    #         loss_function= SoftDiceclDiceLoss(iter_ = 20, alpha = 0.15),
    #         image_key= 'image', 
    #         label_key= 'label',
    #     )

    # make the checkpoint callback
    best_checkpoint_callback = ModelCheckpoint(
        save_top_k=1,
        monitor="val_loss",
        mode="min",
        dirpath=logdir_path,
        every_n_epochs=2,
        filename="vit-best",
    )
    last_checkpoint_callback = ModelCheckpoint(
        save_top_k=1,
        save_last=True,
        dirpath=logdir_path,
        every_n_epochs=2,
        filename="vit-last",
    )

    # logger
    logger = TensorBoardLogger(
        save_dir=logdir_path, version=1, name="lightning_logs"
    )
    # wandb_logger = WandbLogger(project='e15_unet', log_model=True)
    # sample_batch = next(iter(val_loader))
    # # print(sample_batch.shape)
    # # print(sample_batch['image'].shape)

    # labels_to_log = sample_batch['label'][0,0,50,:]
    # images_to_log = sample_batch['image'][0,0,50,:]
    # print(labels_to_log.shape)
    # wandb_logger.log_image(key="validation", images=[labels_to_log, images_to_log])
    # wandb_logger.experiment.config['batch_size'] = batch_size


    trainer = pl.Trainer(
        accelerator="gpu",
        devices=1,
        callbacks=[best_checkpoint_callback, last_checkpoint_callback],
        logger=logger,
        # logger=wandb_logger,
    )
    trainer.fit(
        unet,
        train_dataloaders=train_loader,
        val_dataloaders=val_loader,
    )
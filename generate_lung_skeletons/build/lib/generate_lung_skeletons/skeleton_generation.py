import raster_geometry as rg
import networkx as nx
import numpy as np
import pyclesperanto_prototype as cle

import trimesh as tri
from scipy.spatial.transform import Rotation as R
from scipy.spatial import KDTree
import skimage
from typing import Tuple
import napari_process_points_and_surfaces as npps
from trimesh.voxel.creation import voxelize
import random
from scipy.ndimage import distance_transform_edt
import raster_geometry as rg
# from skimage.transform import rotate
from scipy.ndimage import rotate
import pyclesperanto_prototype as clp 
def select_points_in_bounding_box(
    points: np.ndarray,
    lower_left_corner: np.ndarray,
    upper_right_corner: np.ndarray,
) -> np.ndarray:
    """From an array of points, select all points inside a specified
    axis-aligned bounding box.
    Parameters
    ----------
    points : np.ndarray
        The n x d array containing the n, d-dimensional points to check.
    lower_left_corner : np.ndarray
        The point corresponding to the corner of the bounding box
        with lowest coordinate values.
    upper_right_corner : np.ndarray
        The point corresponding to the corner of the bounding box
        with the highest coordinate values.
    Returns
    -------
    points_in_box : np.ndarray
        The n x d array containing the n points inside of the
        specified bounding box.
    """
    in_box_mask = np.all(
        np.logical_and(
            lower_left_corner <= points, upper_right_corner >= points
        ),
        axis=1,
    )
    return points[in_box_mask]

def draw_line_segment(
    start_point: np.ndarray,
    end_point: np.ndarray,
    skeleton_image: np.ndarray,
    fill_value: int = 1,
):
    """Draw a line segment in-place.
    Note: line will be clipped if it extends beyond the
    bounding box of the skeleton_image.
    Parameters
    ----------
    start_point : np.ndarray
        (d,) array containing the starting point of the line segment.
        Must be an integer index.
    end_point : np.ndarray
        (d,) array containing the end point of the line segment.
        Most be an integer index
    skeleton_image : np.ndarray
        The image in which to draw the line segment.
        Must be the same dimensionality as start_point and end_point.
    fill_value : int
        The value to use for the line segment.
        Default value is 1.
    """
    branch_length = np.linalg.norm(end_point - start_point)
    n_skeleton_points = int(2 * branch_length)
    skeleton_points = np.linspace(start_point, end_point, n_skeleton_points)

    # filter for points within the image
    image_bounds = np.asarray(skeleton_image.shape) - 1
    skeleton_points = select_points_in_bounding_box(
        points=skeleton_points,
        lower_left_corner=np.array([0, 0, 0]),
        upper_right_corner=image_bounds,
    ).astype(int)
    skeleton_image[
        skeleton_points[:, 0], skeleton_points[:, 1], skeleton_points[:, 2]
    ] = fill_value

def draw_points(
    points: np.ndarray,
    # end_point: np.ndarray,
    skeleton_image: np.ndarray,
    fill_value: int = 1,
):
    """Draw a line segment in-place.
    Note: line will be clipped if it extends beyond the
    bounding box of the skeleton_image.
    Parameters
    ----------
    start_point : np.ndarray
        (d,) array containing the starting point of the line segment.
        Must be an integer index.
    end_point : np.ndarray
        (d,) array containing the end point of the line segment.
        Most be an integer index
    skeleton_image : np.ndarray
        The image in which to draw the line segment.
        Must be the same dimensionality as start_point and end_point.
    fill_value : int
        The value to use for the line segment.
        Default value is 1.
    """
 
    skeleton_points = points

    # filter for points within the image
    image_bounds = np.asarray(skeleton_image.shape) - 1
    skeleton_points = select_points_in_bounding_box(
        points=skeleton_points,
        lower_left_corner=np.array([0, 0, 0]),
        upper_right_corner=image_bounds,
    ).astype(int)
    skeleton_image[
        skeleton_points[:, 0], skeleton_points[:, 1], skeleton_points[:, 2]
    ] = fill_value

def augment_tree(tree):
    #Rotation matrices
    rot_degree = np.radians(random.sample(list(np.linspace(-30,30,60)),3))
    R_matrix_x = np.matrix(([1,0,0],
                            [0,np.cos(rot_degree[0]),-np.sin(rot_degree[0])],
                            [0,np.sin(rot_degree[0]),np.cos(rot_degree[0])]))

    R_matrix_y = np.matrix(([np.cos(rot_degree[1]),0,np.sin(rot_degree[1])],
                            [0,1,0],
                            [-np.sin(rot_degree[1]),0,np.cos(rot_degree[1])]))

    R_matrix_z = np.matrix(([np.cos(rot_degree[2]),-np.sin(rot_degree[2]),0],
                            [np.sin(rot_degree[2]),np.cos(rot_degree[2]),0],
                            [0,0,1]))
    r_x =R.from_matrix(R_matrix_x) 
    r_y = R.from_matrix(R_matrix_y)
    r_z =R.from_matrix(R_matrix_z)
    print(rot_degree)

    #Augment tree by rotating it along the trachea
    pos = nx.get_node_attributes(tree, 'pos')

    update_pos = {}
    for node, node_pos in pos.items():
        root = pos[0]
        p = node_pos - root

        p_rot =r_x.apply(p)
        p_rot =r_y.apply(p_rot)
        p_rot =r_z.apply(p_rot)
        p_rot = p_rot + root
        update_pos[node] = p_rot.flatten()
    nx.set_node_attributes(tree, update_pos, name = 'pos')



def add_rotation_to_tree(tree):
    """Adds a random rotation between -90 and 90 degree to each level of the tree graph in place. 

    Args:
        tree (nx.digraph): nx.digraph tree (no self intersections). 
        Each node needs to contain the 3D positional coordinates (array with shape (3,)) in attribute 'pos'.
        Rotation will happen along axis 1 (y).
    """
    in_edges =list(tree.in_edges())
    in_edges_flatt = [node for edge in in_edges for node in edge]
    for nodes,degree in tree.degree():
        rot_degree = np.radians(random.sample(list(np.linspace(-90,90,10)),1))
        updated_postions = {}
        if degree == 3:
            if nodes in in_edges_flatt:
                sub_tree = tree.subgraph(nx.dfs_tree(tree, source = nodes).nodes())

                root = np.array(dict(sub_tree.nodes(data = True))[nodes]['pos'])

                for node in sub_tree.nodes():
                    p = tree.nodes(data ='pos')[node]
                    p = p - root

                    R_matrix = np.matrix(([np.cos(rot_degree),0,np.sin(rot_degree)],[0,1,0],[-np.sin(rot_degree),0,np.cos(rot_degree)]))
                    r = R.from_matrix(R_matrix) 
                    p_rot = r.apply(p)
                    p_rot = p_rot + root
                    updated_postions[node] = p_rot.flatten()

            nx.set_node_attributes(tree, updated_postions, name = 'pos')

    #avoid negative coordinates
    #move to positive coordinats
    pos_dict  = nx.get_node_attributes(tree, 'pos')
    pos_values =  np.array(list(pos_dict.values()))

    x_shift  = np.abs(np.min(pos_values[:,0])) +10
    y_shift  = np.abs(np.min(pos_values[:,1])) +30
    z_shift  = np.abs(np.min(pos_values[:,2])) +10




    pos = {k:v + np.array([x_shift, y_shift, z_shift]) for k, v in pos_dict.items()}
    nx.set_node_attributes(tree, pos, 'pos')


def graph_to_diluted_image(G,pad_size = 15, include_edge_points = True,dilute_tips = True, branch_point_value = 1, edge_point_value =1, elipse_dilute =False):
    """transforms networkx digraph to image. 
    Returns three 3D images containing the voxelized graph, its branch point value and its endpoints,  respectivley.

    Args:
        G (nx.digraph): graph to be voxelized. Needs 3D euclidian coordinates in node attribute called 'pos'.
        include_edge_points (bool, optional): If True, returns 2 extra label images with the branch points and endpoints. 
                                            CURRENTLY BROKEN. Defaults to True.
        branch_point_value (int, optional): value branch point label image gets. Defaults to 1.
        edge_point_value (int, optional): valuee branch point label image gets. Defaults to 1.

    Returns:
        The (x,y,z) Images containing the skeleton_image, branch_points_image, end_point_image: 
    """


    # pos_array = np.asarray(list(pos.values()))
    #if dilute  tips, include edge points is required
    if dilute_tips == True:
        if include_edge_points == False:
            include_edge_points = True
        
    pos = np.asarray([n for n in dict(G.nodes(data = 'pos')).values()])
    update_level = {}
    for edge in G.edges():
        update_level[edge] = len(nx.shortest_path(G, 0, edge[0]))-1

    nx.set_edge_attributes(G, update_level, 'level')


    edges = np.array(list(G.edges(data = 'level')))



    x_offset = 0
    y_offset = 0
    z_offset = 0  
    x_coord = int(np.ceil(np.max(pos[:,0])  - x_offset))
    y_coord = int(np.ceil(np.max(pos[:,1]) - y_offset))
    z_coord = int(np.ceil(np.max(pos[:,2]) - z_offset))
    skeleton_image = np.pad(np.zeros((x_coord,y_coord,z_coord)), pad_width=pad_size)

    pos_old = pos.copy()
    pos += pad_size

    #draw on image
    fp = skimage.morphology.ball(2)
    if elipse_dilute == True:
        fp = rg.ellipsoid(shape =fp.shape,semiaxes = (1,4,1), smoothing =False )
    skel_image_dil = skeleton_image.copy()
    skel_image =skeleton_image.copy()
    branch_point_image = skeleton_image.copy()
    edge_point_image = skeleton_image.copy()

    num_dil = 1
    starting_level = -1 
    for i in range(edges.shape[0]):
        if starting_level != edges[i][2]:
            if starting_level >1:
                num_dil = 2
            else: 
                num_dil = 4
            print('dilating level', edges[i][2])
            for j in range(num_dil):
                skel_image_dil =skimage.morphology.binary_dilation(skel_image_dil,fp)

            starting_level += 1

        draw_line_segment(pos[edges[i]][0],pos[edges[i]][1],skel_image_dil)
        draw_line_segment(pos[edges[i]][0],pos[edges[i]][1],skel_image)

    branch_p = []
    edge_p =[]
    if include_edge_points == True:
        for n,d in G.degree():
            if d ==3:
                branch_p.append(pos[n])
            elif d == 1:
                edge_p.append(pos[n])
        branch_p = np.asarray(branch_p).astype(int) #-1
        edge_p = np.asarray(edge_p).astype(int)
        
        draw_points(branch_p, branch_point_image, fill_value=branch_point_value)
        draw_points(edge_p, edge_point_image, fill_value=edge_point_value)

    fp2 = skimage.morphology.ball(16)



    if elipse_dilute == True:

        fp2 = rg.ellipsoid(shape =fp2.shape,semiaxes = (2,12,2), smoothing =False )

    #dilute tips
    if dilute_tips == True:
        tip_fp = rg.ellipsoid(shape =fp2.shape,semiaxes = (5,15,5), smoothing =False )
        print("dilute tips")
        edge_point_image_dilute = edge_point_image.copy()
        edge_point_image_dilute = skimage.morphology.binary_dilation(edge_point_image, tip_fp)
        skel_image_dil[edge_point_image_dilute != 0] = 1
    #final dilution
    skel_image_dil = skimage.morphology.binary_dilation(skel_image_dil, fp2)


    return skel_image_dil, skel_image, branch_point_image, edge_point_image
              

def graph_to_image(G,include_edge_points = True, branch_point_value = 1, edge_point_value =1, pad_size = 15):
    """transforms networkx digraph to image. 
    Returns three 3D images containing the voxelized graph, its branch point value and its endpoints,  respectivley.

    Args:
        G (nx.digraph): graph to be voxelized. Needs 3D euclidian coordinates in node attribute called 'pos'.
        include_edge_points (bool, optional): If True, returns 2 extra label images with the branch points and endpoints. 
                                            CURRENTLY BROKEN. Defaults to True.
        branch_point_value (int, optional): value branch point label image gets. Defaults to 1.
        edge_point_value (int, optional): valuee branch point label image gets. Defaults to 1.

    Returns:
        The (x,y,z) Images containing the skeleton_image, branch_points_image, end_point_image: 
    """


    pos_dict = nx.get_node_attributes(G, 'pos')
    # pos_array = np.asarray(list(pos.values()))


    pos = np.asarray([n for n in dict(G.nodes(data = 'pos')).values()])
    edges = np.array(G.edges)


    x_offset = 0
    y_offset = 0
    z_offset = 0  
    x_coord = int(np.ceil(np.max(pos[:,0]) - x_offset))
    y_coord = int(np.ceil(np.max(pos[:,1]) - y_offset))
    z_coord = int(np.ceil(np.max(pos[:,2]) - z_offset))

    skeleton_image = np.zeros((x_coord,y_coord,z_coord))
    branch_point_image = skeleton_image.copy() 
    edge_point_image = skeleton_image.copy() 

    #draw on image
    for i in range(edges.shape[0]):

        draw_line_segment(pos[edges[i]][0],pos[edges[i]][1],skeleton_image)

    branch_p = []
    edge_p =[]
    if include_edge_points == True:
        for n,d in G.degree():
            if d ==3:
                branch_p.append(pos[n])
            elif d == 1:
                edge_p.append(pos[n])
        branch_p = np.asarray(branch_p).astype(int) #-1
        edge_p = np.asarray(edge_p).astype(int)
                

        # branch_p = np.asarray([pos[n] for n,d in G.degree() if d == 3]).astype(int) -1
        # edge_p = np.asarray([pos[n] for n,d in G.degree() if d != 3]).astype(int) 

        draw_points(branch_p, branch_point_image, fill_value=branch_point_value)
        draw_points(edge_p, edge_point_image, fill_value=edge_point_value)

    skeleton_image = np.pad(skeleton_image, pad_width=pad_size)
    branch_point_image = np.pad(branch_point_image, pad_width=pad_size)
    edge_point_image = np.pad(edge_point_image, pad_width=pad_size)        
    return skeleton_image,branch_point_image,edge_point_image,



def dilute_tree_image(G, image, pad_size = 15):
    """Dilute skeleton tree image depending on the level. Lower levels will be diluted stronger.

    Args:
        G (nx.digraph): skeleton image in graph representation. Needs node attribute 'pos' with cartesian coordinates.
        image (ndarry (x,y,z)): skeleton image to be diluted

    Returns:
        ndarry(x,y,z): diluted skeleton image
    """
    pos = np.asarray([n for n in dict(G.nodes(data = 'pos')).values()])
    # edges = np.array(G.edges)

    level_pos = -np.sort(-np.unique(pos[:,1].astype(int)))
    level_pos[0] = int(level_pos[0]) + pad_size
    # level_pos = np.append(level_pos,[0])

    # print(level_pos)

    num_dil = len(level_pos)*3
    fp = rg.sphere(4,2)

    skel_image_dil = image.copy()

    for i in range(len(level_pos)-1):
        overlap = 10

        level_segment = skel_image_dil[:,level_pos[i+1]-overlap:level_pos[i]+overlap,:]
        for j in range(num_dil):
            level_segment = skimage.morphology.binary_dilation(level_segment, fp)
        skel_image_dil[:,level_pos[i+1]-overlap:level_pos[i]+overlap,:] = level_segment
        num_dil = int(np.ceil((num_dil/3))) +2
        print(level_pos[i+1]-overlap,level_pos[i]+overlap)
    
    # fp = rg.sphere(5,3)
    # skel_image_dil = skimage.morphology.binary_dilation(skel_image_dil, fp)

    return skel_image_dil

def augment_image(image_list):
        rot_degree = random.sample(list(np.linspace(-30,30,30)),1)[0]
        rot_axes = random.sample([0,1,2],2)
        print(rot_degree,rot_axes)
        image_rot_list = []
        for image in image_list:
            image_c = image.copy()
            image_rot = rotate(input = image_c, angle = rot_degree, axes = rot_axes, reshape=False, order=0)
            image_rot_list.append(image_rot)

        # branch_point_image_rot = rotate(branch_point_image, rot_degree, axes=rot_axes , reshape=False)
        # end_point_image_rot = rotate(end_point_image, rot_degree, axes = rot_axes, reshape=False)
        
        return image_rot_list


def add_noise_to_image_surface(image, noise_magnitude = 15):
    """Adds noise to the image surface. CURRENTLY REQUIRES MANUAL INSTALLATION OF BINVOX.
        Meshes surface of image and adds gaussian noise to it. Uses multiple smoothing steps.


    Args:
        image (ndarray (x,y,z)): image to add noise to.
        noise_magnitude (int): Magnitude of gaussian displacement. Big values lead to discontinuities in mesh.


    Returns:
        image (ndarray (x,y,z)): Noisy input image
    """

    image_surface = npps.label_to_surface(image)
    image_surface_smooth = npps.filter_smooth_simple(image_surface, 15)
    image_surface_smooth = npps.simplify_vertex_clustering(image_surface_smooth, voxel_size = 4)

    skel_mesh_smooth = tri.Trimesh(image_surface_smooth[0], image_surface_smooth[1])

    skel_mesh_noise = tri.permutate.noise(skel_mesh_smooth, magnitude = noise_magnitude)
    # print(skel_mesh_noise)
    skel_mesh_noise = tri.smoothing.filter_taubin(skel_mesh_noise, lamb  = 0.7)
    
    bounds = np.array(((0,image.shape[0]),(0,image.shape[1]),(0,image.shape[2]))).T
    skel_mesh_vox = voxelize(skel_mesh_noise, pitch = 1, method='binvox', bounds = bounds, binvox_path = '/home/mmederacke/bin/binvox')
    return skel_mesh_vox.matrix

def compute_skeleton_vector_field(
         skeleton_image: np.ndarray,
         segmentation_image: np.ndarray
 ) -> np.ndarray:
     """Compute the vector field pointing towards the nearest
     skeleton point for each voxel in the segmentation.

     Parameters
     ----------
     skeleton_image : np.ndarray
         The image containing the skeleton. Skeleton should
         be a single value greater than zero.
     segmentation_iamge : np.ndarray
         The segmentation corresponding to the skeleton.

     Returns
     -------
     vector_image : np.ndarray
         The (3, m, n, p) image containing the vector field of the
         (m, n, p) segmentation/skeleton image.
         vector_image[0, ...] contains the 0th component of the vector field.
         vector_image[1, ...] contains the 1st component of the vector field.
         vector_image[2, ...] contains the 2nd component of the vector field.
     """
     # get the skeleton coordinates and make a tree
     skeleton_coordinates = np.column_stack(np.where(skeleton_image))
     skeleton_kdtree = KDTree(skeleton_coordinates
                              )

     # get the segmentation coordinates
     segmentation_coordinates = np.column_stack(
         np.where(segmentation_image)
     )

     # get the nearest skeleton point for each
     distance_to_skeleton, nearest_skeleton_point_indices = skeleton_kdtree.query(
         segmentation_coordinates
     )
     nearest_skeleton_point = skeleton_coordinates[nearest_skeleton_point_indices]

     # get the pixels that are not containing the skeleton
     non_skeleton_mask = distance_to_skeleton > 0
     nearest_skeleton_point = nearest_skeleton_point[non_skeleton_mask]
     segmentation_coordinates = segmentation_coordinates[non_skeleton_mask]
     distance_to_skeleton = distance_to_skeleton[non_skeleton_mask]

     # get the vector pointing to the skeleton
     vector_to_skeleton = nearest_skeleton_point - segmentation_coordinates
     unit_vector_to_skeleton = vector_to_skeleton / distance_to_skeleton[:, None]

     # flip and rescale the distance
     normalized_distance = distance_to_skeleton / distance_to_skeleton.max()
     flipped_distance = 1 - normalized_distance

     # scale the vectors by the flipped/normalized distance
     scaled_vector_to_skeleton = unit_vector_to_skeleton * flipped_distance[:, None]

     # embed the vectors into an image
     image_shape = (3,) + skeleton_image.shape
     vector_image = np.zeros(image_shape)
     for dimension_index in range(3):
         vector_image[
             dimension_index,
             segmentation_coordinates[:, 0],
             segmentation_coordinates[:, 1],
             segmentation_coordinates[:, 2]
         ] = scaled_vector_to_skeleton[:, dimension_index]

     return vector_image

def make_segmentation_distance_image_norm(
     segmentation_image: np.ndarray,
    ) -> Tuple[np.ndarray, np.ndarray]:
    """Create images where each voxel describes the relationship
    to the nearest background pixel.

    Parameters
    ----------
    segmentation_image : np.ndarray
        The image containing the segmentation.

    Returns
    -------
    distance_image : np.ndarray
        Image where each voxel is the euclidian distance to the nearest
        background pixel.
    scaled_background_vector_image : np.ndarray
        (3, m, n, p) image where each voxel is a vector pointing
        towards the nearest background pixel.
        The vector magnitude is normalized to the maximum distance.
        vector_image[0, ...] contains the 0th component of the vector field.
        vector_image[1, ...] contains the 1st component of the vector field.
        vector_image[2, ...] contains the 2nd component of the vector field.
    """
    distance_image, background_indices = distance_transform_edt(
        segmentation_image, return_distances=True, return_indices=True)

    # get the vector pointing towards the nearest background
    image_index_image = np.indices(segmentation_image.shape)
    background_vector_image = (background_indices - image_index_image).astype(float)

    # scale the vectors by the max distance
    # compute the max distance image
    max_distance = distance_image.max()  
    ball_radius = min(int(max_distance / 2), 12)
    distances_cle = cle.push(distance_image)
    local_maximum_cle = cle.maximum_sphere(
    distances_cle,
    radius_x=ball_radius,
    radius_y=ball_radius,
    radius_z=ball_radius
    )
    max_distance_image = np.asarray(local_maximum_cle)

    # normalize the vectors by the local max distance
    normalized_vectors = np.zeros_like(background_vector_image)
    normalized_vectors[:, segmentation_image] = background_vector_image[:, segmentation_image] / max_distance_image[segmentation_image]

    return normalized_vectors


def make_segmentation_distance_image(
     segmentation_image: np.ndarray,
 ) -> Tuple[np.ndarray, np.ndarray]:
     """Create images where each voxel describes the relationship
     to the nearest background pixel.

     Parameters
     ----------
     segmentation_image : np.ndarray
         The image containing the segmentation.

     Returns
     -------
     distance_image : np.ndarray
         Image where each voxel is the euclidian distance to the nearest
         background pixel.
     scaled_background_vector_image : np.ndarray
         (3, m, n, p) image where each voxel is a vector pointing
         towards the nearest background pixel.
         The vector magnitude is normalized to the maximum distance.
         vector_image[0, ...] contains the 0th component of the vector field.
         vector_image[1, ...] contains the 1st component of the vector field.
         vector_image[2, ...] contains the 2nd component of the vector field.
     """
     distance_image, background_indices = distance_transform_edt(
         segmentation_image, return_distances=True, return_indices=True
     )

     # get the vector pointing towards the nearest background
     image_index_image = np.indices(segmentation_image.shape)
     background_vector_image = background_indices - image_index_image

     # scale the vectors by the max distance
     max_distance = distance_image.max()
     scaled_background_vector_image = background_vector_image / max_distance

     return distance_image, scaled_background_vector_image


def make_segmentation_distance_image_norm_non_vector(
    segmentation: np.ndarray,
    gpu: bool = False,
) -> np.ndarray:
    """Make a distance image from a segmentation image.

    The distance image is normalized to the range with a local maximum filter[0, 1].

    Parameters
    ----------
    segmentation : np.ndarray
        The segmentation image.

    Returns
    -------
    np.ndarray
        The distance image.
    """
    distance_image, background_indices = distance_transform_edt(segmentation, return_distances=True, return_indices=True)

    max_distance = distance_image.max()  
    ball_radius = min(int(max_distance / 2), 8)
    if gpu == True:
        distances_cle = cle.push(distance_image)
        local_maximum_cle = cle.maximum_sphere(
        distances_cle,
        radius_x=ball_radius,
        radius_y=ball_radius,
        radius_z=ball_radius
        )
    if gpu == False:
        distance_image = distance_image / max_distance

        local_maximum_cle = rg.sphere((ball_radius*2)+4,ball_radius)
        local_maximum_cle = skimage.filters.rank.maximum(distance_image, footprint=local_maximum_cle)

    max_distance_image = np.asarray(local_maximum_cle)

    normalized_distance_image = np.zeros_like(distance_image)
    normalized_distance_image[segmentation] = distance_image[segmentation] / max_distance_image[segmentation]
    return normalized_distance_image


def find_indices_within_radius(
     array_shape: Tuple[int, int, int], center_point: np.ndarray, radius: int
 ) -> np.ndarray:
     index_array = np.indices(array_shape).reshape(len(array_shape), -1).T
     city_block_distance = np.sum(np.abs(index_array - center_point), axis=1)
     within_radius_mask = city_block_distance <= radius
     return index_array[within_radius_mask]
     
def draw_proximal_vector_field(
    vector_image: np.ndarray, point_coordinate: np.ndarray, radius: int
):
    image_shape = tuple(np.asarray(vector_image.shape)[1:])
    point_proximal_indices = find_indices_within_radius(
        array_shape=image_shape, center_point=point_coordinate, radius=radius
    )

    # get vectors pointing towards the point
    point_vectors = point_coordinate[None, :] - point_proximal_indices

    # remove the vectors with magnitude 0 and normalize
    magnitudes = np.linalg.norm(point_vectors, axis=1)
    point_vectors = point_vectors[magnitudes != 0]
    point_proximal_indices = point_proximal_indices[magnitudes != 0]
    magnitudes = magnitudes[magnitudes != 0]

    normalized_point_vectors = point_vectors / magnitudes[:, None]

    # embed the vectors in an image
    for dimension_index in range(3):
        vector_image[
            dimension_index,
            point_proximal_indices[:, 0],
            point_proximal_indices[:, 1],
            point_proximal_indices[:, 2],
        ] = normalized_point_vectors[:, dimension_index]


def make_proximal_vector_image(
    image_shape: Tuple[int, int, int],
    point_coordinates: np.ndarray,
    radius: int,
):

    vector_image_shape = (len(image_shape),) + image_shape
    vector_image = np.zeros(vector_image_shape)
    for point in np.atleast_2d(point_coordinates):
        draw_proximal_vector_field(
            vector_image=vector_image, point_coordinate=point, radius=radius
        )
    return vector_image

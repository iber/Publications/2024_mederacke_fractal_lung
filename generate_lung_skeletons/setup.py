from setuptools import setup

setup(
   name='generate_lung_skeletons',
   version='0.1',
   description='Generates simulated bronchial tree image data',
   author='Malte Mederacke',
   author_email='malte.mederacke@bsse.ethz.ch',
   packages=['generate_lung_skeletons'],  #same as name
#    install_requires=['wheel', 'bar', 'greek'], #external packages as dependencies
)
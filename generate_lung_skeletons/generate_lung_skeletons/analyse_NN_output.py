import os
from typing import Tuple

import napari_process_points_and_surfaces as npps
import trimesh as tri
import igl

import h5py
import napari
import numpy as np
from skimage.feature import blob_log
import networkx as nx

# from skeletolung.graph_cleanup import napari_plot_graph, get_edge_points, get_xyz


def predicted_skeleton_to_graph(skeleton_prediction_image:np.array, branch_point_coordinates, end_point_coordinates, prediction_threshold = 0.3):
    """Take the output of the skeletonization neural network and compute the optimal graph discribing the skeleton.
    The function consists out of 4 steps:
    1. threshold the skeleton prediction image and transfrom it into a graph
    2. deterime the merge distance around each branch and endpoint by meshing the image
        and computing the minmal distance to the surface
    3. merge all nodes close to branch- and endpoints
    4. compute the shortest paths between branch-and endpoints and build skeletonization graph 

    Args:
        skeleton_prediction_image (ndarray(z,y,x)): image of the probability space for the skeleton predicted by the neural network
        branch_point_coordinates (ndarray(i,3)): image coordinates of the branchpoints. i = number of branchpoints
        end_point_coordinates (ndarray(j,3)): image coordinates of the endpoints. j number of endpoints
        prediction_threshold (float): Relative threshold for the skeleton prediction image.
    Returns:
        _type_: _description_
    """
    #threshold prediction to get binary image
    thresh  =np.max(skeleton_prediction_image)*prediction_threshold

    skeleton_prediction_image[skeleton_prediction_image < thresh ] = 0
    skeleton_prediction_image[skeleton_prediction_image >= thresh ] = 1

    #compute geometric graph
    coordinates = np.array(np.argwhere(skeleton_prediction_image)).astype(int)
    indices = [i for i in range(1,len(coordinates)+1)]
    pos = {}
    for i, n in enumerate(indices):
        pos[n] = coordinates[i] 
    graph = nx.random_geometric_graph(n = indices, radius = 1, dim = 3, pos = pos)
    graph.remove_nodes_from(list(nx.isolates(graph)))

    #compute mesh and mesh distance to define merge radius
    mesh = npps.label_to_surface(skeleton_prediction_image)
    mesh = tri.Trimesh(mesh[0],mesh[1])

    signed_distance_bp,_,_ = igl.signed_distance(branch_point_coordinates,mesh.vertices, mesh.faces )
    signed_distance_ep,_,_ = igl.signed_distance(end_point_coordinates,mesh.vertices, mesh.faces )

    #merge points around branch and end points to gurantee graph passing through them 
    #identifying points to merge
    branch_points = []
    end_points= []

    branch_point_coordinates_s = []
    end_point_coordinates_s = []

    for node, positions in graph.nodes(data = 'pos'):
        for bp in branch_point_coordinates:
            if np.allclose(positions.astype(int), bp.astype(int)):
                branch_points.append(node)
                branch_point_coordinates_s.append(positions)
        for ep in end_point_coordinates:
            if np.allclose(positions.astype(int), ep.astype(int)):
                end_points.append(node)
                end_point_coordinates_s.append(positions)

        branch_point_coordinates = np.array(branch_point_coordinates_s)
        end_point_coordinates = np.array(end_point_coordinates_s)

        point_indices_joined = branch_points+end_points

        point_coordinates_joined = np.concatenate((branch_point_coordinates, end_point_coordinates))

        point_distances_joined = np.concatenate([np.atleast_1d(signed_distance_bp), np.atleast_1d(signed_distance_ep)])

        nodes_to_merge = []
        for d, points in zip(point_distances_joined,point_coordinates_joined):
            distances = np.linalg.norm(points - np.array(list(dict(graph.nodes(data = "pos")).values())),axis = 1)
            nodes_to_merge.append(np.argwhere(distances <= d).flatten()+1)
    
    #merge points
    graph_merged = graph.copy()

    for p, ntm in zip(point_indices_joined,nodes_to_merge):
        for j in ntm:
            nx.contracted_nodes(graph_merged, p, j, copy = False, self_loops = False)

    for p, i in enumerate(point_indices_joined):
        nx.set_node_attributes(graph_merged,{i:point_coordinates_joined[p]}, name = 'pos')

    #find shortest path in image from branchpoints to endpoints
    sps =[]
    for bp in branch_points:
        for ep in end_points:
            sps.append(nx.shortest_path(graph_merged,bp,ep))


    #remove all connections including all points. Very slow, maybe do in one step?
    for l in sps:
        if len(np.intersect1d(l, point_indices_joined)) >2:
            sps.remove(l)




    graph_reduced = nx.Graph()

    for sp in sps:
        for i in range(len(sp)-1):
            graph_reduced.add_edge(sp[i],sp[i+1])


    reduced_positions = {}
    for node in graph_reduced.nodes:
        reduced_positions[node]=dict(graph_merged.nodes(data = 'pos'))[node]
    nx.set_node_attributes(graph_reduced,reduced_positions, name ='pos')

    return graph_reduced


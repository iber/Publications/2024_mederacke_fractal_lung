import numpy as np
from generate_lung_skeletons.skeleton_generation import (
    add_rotation_to_tree, 
    augment_tree,
    augment_image,
    graph_to_image,
    draw_points,
    # graph_to_diluted_image,
    dilute_tree_image,
    add_noise_to_image_surface,
    make_segmentation_distance_image_norm
)
from morphospaces.data.data_utils import (
    draw_line_segment,
    find_indices_within_radius,
)
from numpy.random import default_rng

from morphospaces.io.hdf5 import write_multi_dataset_hdf
from morphospaces.data.skeleton import compute_skeleton_vector_field,make_segmentation_distance_image, draw_proximal_vector_field, make_proximal_vector_image, make_skeleton_blur_image, make_point_blur, make_skeletonization_target
from generate_lung_skeletons import hdf5
import pickle
import networkx as nx
import skimage
# from skeletolung.graph_cleanup import napari_plot_graph
import numpy.linalg as LA
from skimage.measure import label, regionprops
import pyclesperanto_prototype as cle
from scipy.ndimage import distance_transform_edt
import pickle
import  napari
# %reload_ext autoreload
# %autoreload 2
from  multiprocessing import Pool
import sys
sys.path.insert(0, '/home/mmederacke/lung_fractal/simulation/code/updated/Lung_fractalisation')

# Now you can import the module
from simulation import Lung
import raster_geometry as rg


def set_positions(graph:nx.Graph, step = None, inital_angle =90):
    """Derived from simulations "plot tree" function.

    Parameters
    ----------
 
    """
    # pos= hierarchy_pos(self.tree, root=0)
    g = graph.copy()


    pos = {}
    side = {}

    length = nx.get_edge_attributes(g, 'length')
    length = {edge:l*100_000  for edge, l  in length.items()}
   
    level = nx.get_edge_attributes(g, 'level')

    pos[1] = np.array([0,220])
    pos[0] = np.array([0,pos[1][1] + length[(0,1)]])
    for layer in nx.bfs_layers(g, 0):
        for node in layer:
            out_edges = list(g.out_edges(node)) 

            if len(out_edges) == 2:

                side[out_edges[0]] = 'left'
                side[out_edges[1]] = 'right'

                for s, edge in enumerate(out_edges):
                    if s ==0:
                        angle = np.radians(-inital_angle *1/(1+level[edge])**0.7)
                        y_change = np.cos(angle) * length[edge]
                        x_change = np.sin(angle) * length[edge] 

                        pos[edge[1]] = pos[node]+ np.array([-x_change,-y_change])
                    else:
                        angle = np.radians(inital_angle * 1/(1+level[edge])**0.7)
                        y_change = np.cos(angle) * length[edge]
                        x_change = np.sin(angle) * length[edge] 
                        pos[edge[1]] = pos[node]+ np.array([-x_change, -y_change])
    updated_postions = {}


    for node, position in pos.items():
        if node > 1:
            root = position
            subtree = g.subgraph(nx.dfs_tree(g, source = node).nodes())
            node_side = side[list(g.in_edges(node))[0]]
                
            rot_degree = np.radians(7 * max(level.values()))
            if step:
                rot_degree= np.radians(1* step)
            if node_side == 'left':
                rot_degree = rot_degree
            else:
                rot_degree = -rot_degree
            for sub_nodes in subtree.nodes:
                p = pos[sub_nodes]
                p = p - root
                
                R_matrix = np.matrix(([np.cos(rot_degree),-np.sin(rot_degree)],[np.sin(rot_degree),np.cos(rot_degree)]))
                p_rot = np.array(np.dot(R_matrix,p))
                p_rot = p_rot +  root
                updated_postions[sub_nodes] = p_rot.flatten()
            pos.update(updated_postions)

    updated_postions[0] = pos[0]
    updated_postions[1] = pos[1]
    print(updated_postions)

    nx.set_node_attributes(graph, updated_postions, 'pos')
    return graph,updated_postions
def draw_line_segment_wiggle(
    start_point: np.ndarray,
    end_point: np.ndarray,
    skeleton_image: np.ndarray,
    fill_value: int = 1,
    wiggle_factor: float = 0.1,
):
    """Draw a line segment in-place.

    Note: line will be clipped if it extends beyond the
    bounding box of the skeleton_image.

    Parameters
    ----------
    start_point : np.ndarray
        (d,) array containing the starting point of the line segment.
        Must be an integer index.
    end_point : np.ndarray
        (d,) array containing the end point of the line segment.
        Most be an integer index
    skeleton_image : np.ndarray
        The image in which to draw the line segment.
        Must be the same dimensionality as start_point and end_point.
    fill_value : int
        The value to use for the line segment.
        Default value is 1.
    """
    branch_length = np.linalg.norm(end_point - start_point)
    n_skeleton_points = int(2 * branch_length)
    frequency = 0.09 * wiggle_factor *branch_length
    amplitude= wiggle_factor * branch_length

    skeleton_points = np.linspace(start_point, end_point, n_skeleton_points)

    # Generate sinusoidal noise
    time_points = np.linspace(0, 1, n_skeleton_points)
    sinusoidal_noise = amplitude * np.sin(2 * np.pi * frequency * time_points)

    # Add sinusoidal noise to the line
    axis = np.random.randint(0, 3)
    skeleton_points[1:-1,axis] += np.random.uniform(-0.5, 0.5, size=n_skeleton_points-2) + sinusoidal_noise[1:-1]
    # skeleton_points[:,1] += np.random.uniform(-wiggle_factor*0.5 *branch_length, wiggle_factor*0.5 *branch_length, size=n_skeleton_points) + sinusoidal_noise
    # skeleton_points[:,2] += np.random.uniform(-wiggle_factor*0.5 *branch_length, wiggle_factor*0.5 *branch_length, size=n_skeleton_points) + sinusoidal_noise


    
    for i in range(1,len(skeleton_points)-1):
        draw_line_segment(skeleton_points[i], skeleton_points[i+1], skeleton_image, fill_value=fill_value)


def graph_to_diluted_image(G,pad_size = 15, include_edge_points = True,dilute_tips = True, branch_point_value = 1, edge_point_value =1, elipse_dilute =False):
    """transforms networkx digraph to image. 
    Returns three 3D images containing the voxelized graph, its branch point value and its endpoints,  respectivley.

    Args:
        G (nx.digraph): graph to be voxelized. Needs 3D euclidian coordinates in node attribute called 'pos'.
        include_edge_points (bool, optional): If True, returns 2 extra label images with the branch points and endpoints. 
                                            CURRENTLY BROKEN. Defaults to True.
        branch_point_value (int, optional): value branch point label image gets. Defaults to 1.
        edge_point_value (int, optional): valuee branch point label image gets. Defaults to 1.

    Returns:
        The (x,y,z) Images containing the skeleton_image, branch_points_image, end_point_image: 
    """


    # pos_array = np.asarray(list(pos.values()))
    #if dilute  tips, include edge points is required
    if dilute_tips == True:
        if include_edge_points == False:
            include_edge_points = True
        
    pos = np.asarray([n for n in dict(G.nodes(data = 'pos')).values()])
    update_level = {}
    for edge in G.edges():
        update_level[edge] = len(nx.shortest_path(G, 0, edge[0]))-1

    nx.set_edge_attributes(G, update_level, 'level')


    edges = np.array(list(G.edges(data = 'level')))



    x_offset = 0
    y_offset = 0
    z_offset = 0  
    x_coord = int(np.ceil(np.max(pos[:,0])  - x_offset))
    y_coord = int(np.ceil(np.max(pos[:,1]) - y_offset))
    z_coord = int(np.ceil(np.max(pos[:,2]) - z_offset))
    skeleton_image = np.pad(np.zeros((x_coord,y_coord,z_coord)), pad_width=pad_size)

    pos_old = pos.copy()
    pos += pad_size

    #draw on image
    fp_size = np.random.randint(2,6)
    fp = skimage.morphology.ball(fp_size)
    if elipse_dilute == True:
        fp = rg.ellipsoid(shape =fp.shape,semiaxes = (1,fp_size -1,1), smoothing =False )
    skel_image_dil = skeleton_image.copy()
    skel_image =skeleton_image.copy()
    branch_point_image = skeleton_image.copy()
    edge_point_image = skeleton_image.copy()

    num_dil = 1
    starting_level = -1 
    for i in range(edges.shape[0]):
        if starting_level != edges[i][2]:
            if starting_level >1:
                num_dil = 3 +np.random.randint(4)
            else: 
                num_dil = 4 + np.random.randint(4)
            print('dilating level', edges[i][2])
            for j in range(num_dil):
                skel_image_dil =skimage.morphology.binary_dilation(skel_image_dil,fp)

            starting_level += 1
        wiggle_factor = np.random.uniform(0.03,0.1)
        draw_line_segment_wiggle(pos[edges[i]][0],pos[edges[i]][1],skel_image,wiggle_factor=wiggle_factor)
        skel_image_dil[skel_image == True] = True
    branch_p = []
    edge_p =[]
    if include_edge_points == True:
        for n,d in G.degree():
            if d ==3:
                branch_p.append(pos[n])
            elif d == 1:
                edge_p.append(pos[n])
        branch_p = np.asarray(branch_p).astype(int) #-1
        edge_p = np.asarray(edge_p).astype(int)
        
        draw_points(branch_p, branch_point_image, fill_value=branch_point_value)
        draw_points(edge_p, edge_point_image, fill_value=edge_point_value)

    fp2 = skimage.morphology.ball(14)



    if elipse_dilute == True:

        fp2 = rg.ellipsoid(shape =fp2.shape,semiaxes = (5,15,5), smoothing =False )

    #dilute tips
    if dilute_tips == True:
        tip_fp = rg.ellipsoid(shape =fp2.shape,semiaxes = (5,14,5), smoothing =False )
        print("dilute tips")
        edge_point_image_dilute = edge_point_image.copy()
        edge_point_image_dilute = skimage.morphology.binary_dilation(edge_point_image, tip_fp)
        skel_image_dil[edge_point_image_dilute != 0] = 1
    #final dilution
    skel_image_dil = skimage.morphology.binary_dilation(skel_image_dil, fp2)
    # skel_image_dil = skimage.morphology.binary_dilation(skel_image_dil, fp2)


    return skel_image_dil, skel_image, branch_point_image, edge_point_image
            




def make_segmentation_distance_image_norm_non_vector(
    segmentation: np.ndarray,
) -> np.ndarray:
    """Make a distance image from a segmentation image.

    The distance image is normalized to the range with a local maximum filter[0, 1].

    Parameters
    ----------
    segmentation : np.ndarray
        The segmentation image.

    Returns
    -------
    np.ndarray
        The distance image.
    """
    distance_image, background_indices = distance_transform_edt(segmentation, return_distances=True, return_indices=True)

    max_distance = distance_image.max()  
    ball_radius = min(int(max_distance / 2), 8)

    distances_cle = cle.push(distance_image)
    local_maximum_cle = cle.maximum_sphere(
    distances_cle,
    radius_x=ball_radius,
    radius_y=ball_radius,
    radius_z=ball_radius
    )
    max_distance_image = np.asarray(local_maximum_cle)

    normalized_distance_image = np.zeros_like(distance_image)
    normalized_distance_image[segmentation] = distance_image[segmentation] / max_distance_image[segmentation]
    return normalized_distance_image

# #load tree graph
# with open("Lung_graph2d_2.pickle",'rb') as f:
#     lung = pickle.load(f)


# lung,_ = set_positions(lung, step = 40)
# pos = nx.get_node_attributes(lung, 'pos')
# pos = {k:np.concatenate((v,np.array([0]))) for k,v in pos.items()}
# nx.set_node_attributes(lung, pos, 'pos')

def create_skeletons(name):
    # i = 1
    lung = Lung(depth = 2, partial_fractal_depth=1)
    for u,v ,length in lung.tree.edges(data = 'length'):
        #add noise to length
        lung.tree[u][v]['length'] = (length*1.8) +np.random.random() *length
    y = False
    if y  == True:
        angle = np.random.randint(10,130)
        lung,pos = set_positions(lung.tree, step = 90, inital_angle=angle)
    else: 
        lung,pos = set_positions(lung.tree, step = 90, inital_angle=145)

    pos = nx.get_node_attributes(lung, 'pos')
    pos = {k:np.concatenate((v,np.array([0]))) for k,v in pos.items()}
    nx.set_node_attributes(lung, pos, 'pos')
    lungc = lung.copy()

    #rotate subtrees
    print('augment tree')
    augment_tree(lungc)
    print('rotate tree') 
    add_rotation_to_tree(lungc)
    #rotate whole tree
    # augment_tree(lungc)
    #create image from graph
    pad_size = 40

    # skel_image,branch_point_image,edge_point_image = graph_to_image(lungc, pad_size = pad_size)
    #add a small boarder, so dilituion doesnt fail at the boundaries
    skel_image_dils, skel_image, _, _ = graph_to_diluted_image(lungc, pad_size = pad_size, elipse_dilute= True, dilute_tips=True)



    #resacle image to be squared. Otherwise mesh voxelation shifts image.
    # rescale_factor = np.array([skel_image.shape[1]/(skel_image.shape[0]), 
    #                         skel_image.shape[1]/skel_image.shape[1], 
    #                         skel_image.shape[1]/skel_image.shape[2]])

    #dilute tree image to get a volume
    rescaled_shape= [np.min(skel_image.shape[1])]*3
    skel_image = skimage.transform.resize(skel_image,rescaled_shape, order=1)
    skel_image_dils = skimage.transform.resize(skel_image_dils,rescaled_shape, order=0)

    
    # #rotate for augmentaiton
    # skel_image_dils, branch_point_image, edge_point_image = augment_image(skel_image_dils, branch_point_image, edge_point_image)
    skeleton_mask = np.zeros_like(skel_image_dils, dtype=bool)
    skeleton_mask[skel_image_dils > 0] = 1
    #get branch and edge points

    #add noise to surface to get proper bumps and a more continuouse surface
    skel_mesh_vox = add_noise_to_image_surface(skeleton_mask)

    

    #augment size
    scale_factor = np.random.random(1) +0.25
    skel_mesh_vox = skimage.transform.rescale(skel_mesh_vox, scale_factor, order=0)
    skel_image = skimage.transform.rescale(skel_image, scale_factor, order=0)

    skeleton_gaussian_size = 0
    skeleton_dilation_size = 0
    print(skeleton_dilation_size)

    skeletonization_target =make_skeleton_blur_image(skeleton_image= skel_image,
                                                    gaussian_size = skeleton_gaussian_size,
                                                    dilation_size = skeleton_dilation_size)

    normalized_vector_background_image = make_segmentation_distance_image_norm_non_vector(skel_mesh_vox)

    #segmentation label image
    segmentation_label_image = np.zeros(skel_image.shape)
    segmentation_label_image[skel_mesh_vox != 0] = 1
    segmentation_label_image[skeletonization_target > 0.6] = 2
    segmentation_label_image[skel_mesh_vox == 0] = 0
    segmentation_label_image = segmentation_label_image.astype(np.uint8)
   
    #check for size
    if segmentation_label_image.shape[0] < 160:
        #resize to 160
        segmentation_label_image = skimage.transform.resize(segmentation_label_image, (160,160,160), order=0)
        normalized_vector_background_image = skimage.transform.resize(normalized_vector_background_image, (160,160,160), order=2)
    #safe file
    index = name
    print(name)
    # hdf5.write_multi_dataset_hdf(file_path = f'/home/mmederacke/lung_fractal/ml/synthetic_trainings_data/simulated_trees_distance_only/lung_skeleton_rotated_elipse_{index}.h5',
    #                             skeleton_image = skel_image,
    #                             segmentation_image =  skel_mesh_vox,
    #                             skeletonization_target=skeletonization_target,
    #                             normalized_vector_background_image = normalized_vector_background_image,
    #                             segmentation_label_image = segmentation_label_image,
    
    
    hdf5.write_multi_dataset_hdf(file_path = f'/home/mmederacke/lung_fractal/ml/synthetic_trainings_data/simulated_t/simulated_t{index}.h5',
                                raw = normalized_vector_background_image,
                                label = segmentation_label_image,
                            
                                
                                    )

if __name__ == '__main__':
    pool = Pool()
    pool.map(create_skeletons, np.arange(1,200))
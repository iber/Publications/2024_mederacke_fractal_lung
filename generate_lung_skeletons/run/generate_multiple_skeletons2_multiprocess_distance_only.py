import numpy as np
from generate_lung_skeletons.skeleton_generation import (
    add_rotation_to_tree, 
    augment_tree,
    augment_image,
    graph_to_image,
    graph_to_diluted_image,
    dilute_tree_image,
    add_noise_to_image_surface,
    make_segmentation_distance_image_norm
)
from morphospaces.data.data_utils import (
    draw_line_segment,
    find_indices_within_radius,
)
from numpy.random import default_rng

from morphospaces.io.hdf5 import write_multi_dataset_hdf
from morphospaces.data.skeleton import compute_skeleton_vector_field,make_segmentation_distance_image, draw_proximal_vector_field, make_proximal_vector_image, make_skeleton_blur_image, make_point_blur, make_skeletonization_target
from generate_lung_skeletons import hdf5
import pickle
import networkx as nx
import skimage
# from skeletolung.graph_cleanup import napari_plot_graph
import numpy.linalg as LA
from skimage.measure import label, regionprops
import pyclesperanto_prototype as cle
from scipy.ndimage import distance_transform_edt
import pickle
import  napari
# %reload_ext autoreload
# %autoreload 2
from  multiprocessing import Pool

def set_positions(graph:nx.Graph, step = None):
    """Derived from simulations "plot tree" function.

    Parameters
    ----------
 
    """
    # pos= hierarchy_pos(self.tree, root=0)
    g = graph.copy()


    pos = {}
    side = {}

    length = nx.get_edge_attributes(g, 'length')
    length = {edge:l*100_000  for edge, l  in length.items()}
   
    level = nx.get_edge_attributes(g, 'level')

    pos[1] = np.array([0,220])
    pos[0] = np.array([0,pos[1][1] + length[(0,1)]])
    for layer in nx.bfs_layers(g, 0):
        for node in layer:
            out_edges = list(g.out_edges(node)) 

            if len(out_edges) == 2:

                side[out_edges[0]] = 'left'
                side[out_edges[1]] = 'right'

                for s, edge in enumerate(out_edges):
                    if s ==0:
                        angle = np.radians(-90 *1/(1+level[edge])**0.7)
                        y_change = np.cos(angle) * length[edge]
                        x_change = np.sin(angle) * length[edge] 

                        pos[edge[1]] = pos[node]+ np.array([-x_change,-y_change])
                    else:
                        angle = np.radians(90 * 1/(1+level[edge])**0.7)
                        y_change = np.cos(angle) * length[edge]
                        x_change = np.sin(angle) * length[edge] 
                        pos[edge[1]] = pos[node]+ np.array([-x_change, -y_change])
    updated_postions = {}


    for node, position in pos.items():
        if node > 1:
            root = position
            subtree = g.subgraph(nx.dfs_tree(g, source = node).nodes())
            node_side = side[list(g.in_edges(node))[0]]
                
            rot_degree = np.radians(7 * max(level.values()))
            if step:
                rot_degree= np.radians(1* step)
            if node_side == 'left':
                rot_degree = rot_degree
            else:
                rot_degree = -rot_degree
            for sub_nodes in subtree.nodes:
                p = pos[sub_nodes]
                p = p - root
                
                R_matrix = np.matrix(([np.cos(rot_degree),-np.sin(rot_degree)],[np.sin(rot_degree),np.cos(rot_degree)]))
                p_rot = np.array(np.dot(R_matrix,p))
                p_rot = p_rot +  root
                updated_postions[sub_nodes] = p_rot.flatten()
            pos.update(updated_postions)

    updated_postions[0] = pos[0]
    updated_postions[1] = pos[1]
    # print(updated_postions)

    nx.set_node_attributes(graph, updated_postions, 'pos')
    return graph,updated_postions



def make_segmentation_distance_image_norm_non_vector(
    segmentation: np.ndarray,
) -> np.ndarray:
    """Make a distance image from a segmentation image.

    The distance image is normalized to the range with a local maximum filter[0, 1].

    Parameters
    ----------
    segmentation : np.ndarray
        The segmentation image.

    Returns
    -------
    np.ndarray
        The distance image.
    """
    distance_image, background_indices = distance_transform_edt(segmentation, return_distances=True, return_indices=True)

    max_distance = distance_image.max()  
    ball_radius = min(int(max_distance / 2), 8)

    distances_cle = cle.push(distance_image)
    local_maximum_cle = cle.maximum_sphere(
    distances_cle,
    radius_x=ball_radius,
    radius_y=ball_radius,
    radius_z=ball_radius
    )
    max_distance_image = np.asarray(local_maximum_cle)

    normalized_distance_image = np.zeros_like(distance_image)
    normalized_distance_image[segmentation] = distance_image[segmentation] / max_distance_image[segmentation]
    return normalized_distance_image

#load tree graph
with open("Lung_graph2d_2.pickle",'rb') as f:
    lung = pickle.load(f)


lung,_ = set_positions(lung, step = 40)
pos = nx.get_node_attributes(lung, 'pos')
pos = {k:np.concatenate((v,np.array([0]))) for k,v in pos.items()}
nx.set_node_attributes(lung, pos, 'pos')

def create_skeletons(name):
    # i = 1
    lungc = lung.copy()
    #rotate subtrees
    print('augment tree')
    augment_tree(lungc)
    print('rotate tree') 
    add_rotation_to_tree(lungc)
    #rotate whole tree
    # augment_tree(lungc)
    #create image from graph
    pad_size = 40

    # skel_image,branch_point_image,edge_point_image = graph_to_image(lungc, pad_size = pad_size)
    #add a small boarder, so dilituion doesnt fail at the boundaries
    skel_image_dils, skel_image, _, _ = graph_to_diluted_image(lungc, pad_size = pad_size, elipse_dilute= True, dilute_tips=True)



    #resacle image to be squared. Otherwise mesh voxelation shifts image.
    # rescale_factor = np.array([skel_image.shape[1]/(skel_image.shape[0]), 
    #                         skel_image.shape[1]/skel_image.shape[1], 
    #                         skel_image.shape[1]/skel_image.shape[2]])

    #dilute tree image to get a volume
    rescaled_shape= [np.min(skel_image.shape[1])]*3
    skel_image = skimage.transform.resize(skel_image,rescaled_shape, order=1)
    skel_image_dils = skimage.transform.resize(skel_image_dils,rescaled_shape, order=0)

    
    # #rotate for augmentaiton
    # skel_image_dils, branch_point_image, edge_point_image = augment_image(skel_image_dils, branch_point_image, edge_point_image)
    skeleton_mask = np.zeros_like(skel_image_dils, dtype=bool)
    skeleton_mask[skel_image_dils > 0] = 1
    #get branch and edge points

    #add noise to surface to get proper bumps and a more continuouse surface
    skel_mesh_vox = add_noise_to_image_surface(skeleton_mask)

    

    #augment size
    scale_factor = np.random.random(1) +0.25
    skel_mesh_vox = skimage.transform.rescale(skel_mesh_vox, scale_factor, order=0)
    skel_image = skimage.transform.rescale(skel_image, scale_factor, order=0)

    skeleton_gaussian_size = 1
    skeleton_dilation_size = 1
    print(skeleton_dilation_size)

    skeletonization_target =make_skeleton_blur_image(skeleton_image= skel_image,
                                                    gaussian_size = skeleton_gaussian_size,
                                                    dilation_size = skeleton_dilation_size)

    normalized_vector_background_image = make_segmentation_distance_image_norm_non_vector(skel_mesh_vox)

    #segmentation label image
    segmentation_label_image = np.zeros(skel_image.shape)
    segmentation_label_image[skel_mesh_vox != 0] = 1
    segmentation_label_image[skeletonization_target > 0.6] = 2
    segmentation_label_image[skel_mesh_vox == 0] = 0
    segmentation_label_image = segmentation_label_image.astype(np.uint8)

    #safe file
    index = name
    print(name)
    # hdf5.write_multi_dataset_hdf(file_path = f'/home/mmederacke/lung_fractal/ml/synthetic_trainings_data/simulated_trees_distance_only/lung_skeleton_rotated_elipse_{index}.h5',
    #                             skeleton_image = skel_image,
    #                             segmentation_image =  skel_mesh_vox,
    #                             skeletonization_target=skeletonization_target,
    #                             normalized_vector_background_image = normalized_vector_background_image,
    #                             segmentation_label_image = segmentation_label_image,
    hdf5.write_multi_dataset_hdf(file_path = f'/home/mmederacke/lung_fractal/ml/synthetic_trainings_data/simulated_trees_distance_only_undersegmented_2/lung_skeleton_rotated_elipse_undersegmented_{index}.h5',
                                raw = normalized_vector_background_image,
                                label = segmentation_label_image,
                            
                                
                                    )

# create_skeletons(1)
if __name__ == '__main__':
    pool = Pool()
    pool.map(create_skeletons, np.arange(1,200))
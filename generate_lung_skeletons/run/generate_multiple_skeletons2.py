import numpy as np
from generate_lung_skeletons.skeleton_generation import (
    add_rotation_to_tree, 
    augment_tree,
    augment_image,
    graph_to_image,
    graph_to_diluted_image,
    dilute_tree_image,
    add_noise_to_image_surface,
    make_segmentation_distance_image_norm
)
from morphospaces.data.data_utils import (
    draw_line_segment,
    find_indices_within_radius,
)
from numpy.random import default_rng

from morphospaces.io.hdf5 import write_multi_dataset_hdf
from morphospaces.data.skeleton import compute_skeleton_vector_field,make_segmentation_distance_image, draw_proximal_vector_field, make_proximal_vector_image, make_skeleton_blur_image, make_point_blur, make_skeletonization_target
from generate_lung_skeletons import hdf5
import pickle
import networkx as nx
import skimage
from skeletolung.graph_cleanup import napari_plot_graph
import numpy.linalg as LA
from skimage.measure import label, regionprops

import  napari
# %reload_ext autoreload
# %autoreload 2


#load tree graph
lung = nx.read_gpickle("Lung_graph2d_2.pickle")
pos = nx.get_node_attributes(lung, 'pos')
pos = {k:np.concatenate((v,np.array([0]))) for k,v in pos.items()}

nx.set_node_attributes(lung, pos, 'pos')
for i in range(2,200):
# i = 1
    lungc = lung.copy()
    #rotate subtrees
    augment_tree(lungc)
    add_rotation_to_tree(lungc)
    #rotate whole tree
    # augment_tree(lungc)
    #create image from graph
    pad_size = 40

    # skel_image,branch_point_image,edge_point_image = graph_to_image(lungc, pad_size = pad_size)
    #add a small boarder, so dilituion doesnt fail at the boundaries
    skel_image_dils, skel_image, branch_point_image, edge_point_image = graph_to_diluted_image(lungc, pad_size = pad_size)



    #resacle image to be squared. Otherwise mesh voxelation shifts image.
    rescale_factor = np.array([skel_image.shape[1]/(skel_image.shape[0]), 
                            skel_image.shape[1]/skel_image.shape[1], 
                            skel_image.shape[1]/skel_image.shape[2]])

    #dilute tree image to get a volume
    rescaled_shape= [np.min(skel_image.shape[1])]*3
    skel_image = skimage.transform.resize(skel_image,rescaled_shape, order=1)
    skel_image_dils = skimage.transform.resize(skel_image_dils,rescaled_shape, order=0)

    branch_point_image =skimage.transform.resize(branch_point_image,rescaled_shape, order=0)
    edge_point_image = skimage.transform.resize(edge_point_image, rescaled_shape, order = 0 )
    #its just faster...
    branch_point_image[branch_point_image != 0] = 1
    branch_point_image = label(branch_point_image)
    bp_regions = regionprops(branch_point_image)

    branch_points = np.array([p.centroid for p in bp_regions]).astype(int)

    edge_point_image[edge_point_image != 0] = 1
    edge_point_image = label(edge_point_image)
    ep_regions = regionprops(edge_point_image)

    end_points = np.array([p.centroid for p in ep_regions]).astype(int)

    # #rotate for augmentaiton
    # skel_image_dils, branch_point_image, edge_point_image = augment_image(skel_image_dils, branch_point_image, edge_point_image)
    skeleton_mask = np.zeros_like(skel_image_dils, dtype=bool)
    skeleton_mask[skel_image_dils > 0] = 1
    #get branch and edge points

    #add noise to surface to get proper bumps and a more continuouse surface
    skel_mesh_vox = add_noise_to_image_surface(skeleton_mask)

    rng = default_rng(4)

    skeleton_dilation_size = rng.integers(1,3)
    skeleton_gaussian_size = int(1)
    # skeleton_dilation_size = 7
    point_gaussian_size =int(1) 


    skeletonization_target = make_skeletonization_target(
        skeleton_image = skel_image, 
        skeleton_gaussian_size=skeleton_gaussian_size,
        skeleton_dilation_size =skeleton_dilation_size, 
        end_points=end_points.astype(int),
        branch_points = branch_points.astype(int),
        point_gaussian_size = point_gaussian_size
    )

    branch_point_vector_image= make_proximal_vector_image(
        image_shape = skel_image.shape,
        point_coordinates = branch_points,
        radius = 4
    )
    end_point_vector_image = make_proximal_vector_image(
        image_shape = skel_image.shape, 
        point_coordinates = end_points,
        radius = 4,
    )

    vector_skeleton = compute_skeleton_vector_field(skeleton_image = skel_image,segmentation_image = skel_mesh_vox )

    vector_image = np.concatenate(
        (
            vector_skeleton, 
            branch_point_vector_image, 
            end_point_vector_image
        ),
        axis = 0
    )
    vector_image[:, np.logical_not(skel_mesh_vox)] = 0

    segmentation_distance, scaled_background_vector_image = make_segmentation_distance_image(skel_mesh_vox)

    normalized_vector_background_image = make_segmentation_distance_image_norm(skel_mesh_vox)

        #safe file
    index = i
    print(i)
    hdf5.write_multi_dataset_hdf(file_path = f'../../simulated_trees_augmented_3/lung_skeleton_rotated_{index}.h5',
                                skeleton_image = skel_image,
                                segmentation_image =  skel_mesh_vox,
                                branch_point = branch_points,
                                end_points = end_points,
                                skeleton_vector_image = vector_image,
                                segmentation_distance_image = segmentation_distance,
                                background_vector_image = scaled_background_vector_image,
                                skeletonization_target=skeletonization_target,
                                normalized_vector_background_image = normalized_vector_background_image

                            
                                
                                    )


import numpy as np
from generate_lung_skeletons.skeleton_generation import (
    add_rotation_to_tree, 
    augment_tree,
    augment_image,
    graph_to_image, 
    dilute_tree_image,
    graph_to_diluted_image, 
    add_noise_to_image_surface
)
from morphospaces.data.data_utils import (
    draw_line_segment,
    find_indices_within_radius,
)
from numpy.random import default_rng

from morphospaces.io.hdf5 import write_multi_dataset_hdf
from morphospaces.data.skeleton import compute_skeleton_vector_field,make_segmentation_distance_image, draw_proximal_vector_field, make_proximal_vector_image, make_skeleton_blur_image, make_point_blur, make_skeletonization_target
from generate_lung_skeletons import hdf5
import pickle
import networkx as nx
import skimage


#load tree graph
lung = nx.read_gpickle("/home/mmederacke/lung_fractal/ml/generate_lung_skeletons/run/lung_graph2d.pickle")

#add/update node positions
min_x = np.floor(np.min(np.array(list(dict(lung.nodes(data = 'pos')).values()))[:,0]))
min_y = np.floor(np.min(np.array(list(dict(lung.nodes(data = 'pos')).values()))[:,1]))

node_update = {}
for n in lung.nodes():
    if n == -1:
        node_update[n] = 0
    if n == 0:
        node_update[n] = 1

lung = nx.relabel_nodes(lung, node_update)

# update_pos = {}
# for n in lung.nodes(data ='pos'):
#     node = n[0]
#     if node <= 0:
#         node=node+1
#     update_pos[node] = np.array(n[1]) - np.array((min_x, min_y, 0))
# nx.set_node_attributes(lung, update_pos, name = 'pos')

for i in range(1,200):
    lungc = lung.copy()
    #rotate subtrees
    add_rotation_to_tree(lungc)
    #rotate whole tree
    augment_tree(lungc)
    #create image from graph
    skel_image,branch_point_image,edge_point_image = graph_to_image(lungc)
    #add a small boarder, so dilituion doesnt fail at the boundaries

    pad_size = 30
    #must be in this order!
    skel_image = np.pad(skel_image, pad_width=pad_size)
    branch_point_image = np.pad(branch_point_image, pad_width=pad_size)
    edge_point_image = np.pad(edge_point_image, pad_width=pad_size)

    skel_image_dils = graph_to_diluted_image(lungc, pad_width = pad_size)

    #resacle image to be squared. Otherwise mesh voxelation shifts image.
    rescale_factor = np.array([skel_image.shape[1]/(skel_image.shape[0]), 
                            skel_image.shape[1]/skel_image.shape[1], 
                            skel_image.shape[1]/skel_image.shape[2]])


    rescaled_shape= [np.min(skel_image.shape[1])]*3
    skel_image = skimage.transform.resize(skel_image,rescaled_shape, order=1)
    skel_image_dils = skimage.transform.resize(skel_image_dils*1,rescaled_shape, order=1)
    skel_image_dils[skel_image_dils != 0] = 1
    #dilute tree image to get a volume
    # skel_image_dils = dilute_tree_image(lungc, skel_image)
    
    # #rotate for augmentaiton
    # skel_image_dils, branch_point_image, edge_point_image = augment_image(skel_image_dils, branch_point_image, edge_point_image)

    #get branch and edge points
    branch_points = np.argwhere(branch_point_image != 0) *rescale_factor
    # branch_points= branch_points + np.array([50]*3)

    end_points = np.argwhere(edge_point_image != 0) *rescale_factor
    # end_points= end_points + np.array([50]*3)

    #add noise to surface to get proper bumps and a more continuouse surface
    skel_mesh_vox = add_noise_to_image_surface(skel_image_dils)


    vector_skeleton = compute_skeleton_vector_field(skeleton_image = skel_image,segmentation_image = skel_mesh_vox )

    branch_point_vector_image= make_proximal_vector_image(
        image_shape = skel_image.shape,
        point_coordinates = branch_points,
        radius = 4
    )
    end_point_vector_image = make_proximal_vector_image(
        image_shape = skel_image.shape, 
        point_coordinates = end_points,
        radius = 4,
    )
    vector_image = np.concatenate(
        (
            vector_skeleton, 
            branch_point_vector_image, 
            end_point_vector_image
        ),
        axis = 0
    )
    vector_image[:, np.logical_not(skel_mesh_vox)] = 0

    segmentation_distance, scaled_background_vector_image = make_segmentation_distance_image(skel_mesh_vox)

    rng = default_rng(4)
    skeleton_dilation_size = rng.integers(1,3)
    skeleton_gaussian_size = int(1)
    # skeleton_dilation_size = 7
    point_gaussian_size =int(1) 

    skeletonization_target = make_skeletonization_target(
        skeleton_image = skel_image, 
        skeleton_gaussian_size=skeleton_gaussian_size,
        skeleton_dilation_size =skeleton_dilation_size, 
        end_points=end_points.astype(int),
        branch_points = branch_points.astype(int),
        point_gaussian_size = point_gaussian_size
    )
    #safe file
    index = i
    print(i)
    hdf5.write_multi_dataset_hdf(file_path = f'/home/mmederacke/lung_fractal/ml/simulated_trees_augmented_2/lung_skeleton_rotated_{index}.h5',
                                skeleton_image = skel_image,
                                segmentation_image =  skel_mesh_vox,
                                branch_point = branch_points,
                                end_points = end_points,
                                skeleton_vector_image = vector_image,
                                segmentation_distance_image = segmentation_distance,
                                background_vector_image = scaled_background_vector_image,
                                skeletonization_target=skeletonization_target
                            
                                
                                    )
import numpy as np
import h5py
# import dask.array as da
import skimage as ski
from skimage.exposure import equalize_adapthist
from dask_image import ndinterp

import dask.array as da



# import SimpleITK as sitk
# Load image
image_path = '/path/to/image.h5'
output_path = '/path/to/output.h5'
with h5py.File(image_path, 'r') as f:
    image = f['image'][:]


#resize
print('resizing')
scale_factor = 0.65


image = da.from_array(image, chunks=(400,400,400))
#rescale
output_shape = (np.array(image.shape) *scale_factor).astype(int)
image = ndinterp.affine_transform(image, matrix = np.eye(3)*(1/scale_factor), order = 2, output_shape =output_shape).compute()

with h5py.File(output_path, 'w') as f:
    f.create_dataset('image', data=image, compression='gzip')
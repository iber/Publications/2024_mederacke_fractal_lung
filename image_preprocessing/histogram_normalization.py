import numpy as np
import h5py
import skimage as ski
from skimage.exposure import equalize_adapthist

import dask.array as da

# Load image
image_path = '/path/to/image.h5'
output_path = '/path/to/output.h5'

with h5py.File(image_path,'r') as f:
    image = f['image'][:]
    print(image.shape)


#rescaling
print("rescaling")

# Rescale image data to range [0, 1]
image_eq = np.clip(image,
                  np.percentile(image, 1),
                  np.percentile(image, 99))
image_eq = (image_eq - image_eq.min()) / (image_eq.max() - image_eq.min())

# Set parameters for AHE
# Determine kernel sizes in each dim relative to image shape
kernel_size = (image_eq.shape[0] // 6,
               image_eq.shape[1] // 6,
               image_eq.shape[2] // 6)

image_eq = da.from_array(image_eq, chunks = tuple([i*2 for i in kernel_size]))
print(f"kernel size:{kernel_size}")
#equalize image
print("equalizing")
image_eq = da.map_overlap(equalize_adapthist, image_eq, clip_limit = 0.025, kernel_size = kernel_size, depth = tuple([i//2 for i in kernel_size]))
image_eq = image_eq.compute()


print("saving")
#save image
with h5py.File(output_path, 'w') as f:
    f.create_dataset('image_norm', data=image_eq, compression='gzip')
    f.create_dataset('image' , data=image, compression='gzip')



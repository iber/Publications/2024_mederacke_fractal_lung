#prepare for UNET with dask and GPU

import h5py
import numpy as np
import pyclesperanto_prototype as cle
import dask.array as da
from dask_image.ndmorph import binary_erosion, binary_dilation
import skimage as ski



from scipy.ndimage import distance_transform_edt


def gaussian_blurr(image, sigma):
    return ski.filters.gaussian(image, sigma=sigma)

def distance_transform(img):
    print(img.shape)
    return distance_transform_edt(img, return_distances=True, return_indices=False)

def max_dist_gpu(img, ball_radius = ball_radius):
    local_maximum_cle = cle.maximum_sphere(
        img,
        radius_x=ball_radius,
        radius_y=ball_radius,
        radius_z=ball_radius
        )
    print(local_maximum_cle.shape)

    local_maximum = np.asarray(local_maximum_cle)

    return local_maximum


file_path_input = '/path/to/input.h5'
file_path_output = 'path/to/output.h5'


key = 'segmentation'
smooth = False

#load and  daskified vector image

with h5py.File(file_path_input, 'r') as f:
    img = f[key][:]
print(img.shape)


#make sure to boolean
img = img != 0
img_da = da.from_array(img, chunks=(512, 512, 512))

#smooth
if smooth == True:
    print('smoothing')

    fp = np.ones((8,8,8))
    img = binary_erosion(binary_dilation(img_da, structure=fp), structure=fp).compute()

    img = img != 0
    img_da = da.from_array(img, chunks=(512, 512, 512))


    img_da = img_da.map_overlap(gaussian_blurr, sigma=2, depth=15).compute()

    img_da = da.from_array(img_da > 0.4, chunks=(512, 512, 512))




#adapt depth to diameter of branches...
distance_image = img_da.map_overlap(distance_transform, depth=30, boundary='none').compute()

distance_image_da = da.from_array(distance_image, chunks=(650,650,650))
max_distance = np.max(distance_image)
print(max_distance)
ball_radius = min(int(max_distance / 2), 6)



#depth depends on GPU memory, too
local_maximum_image = distance_image_da.map_overlap(max_dist_gpu, depth=ball_radius*2, boundary = 'none', dtype=np.float32).compute()
print(local_maximum_image.shape)

#normalize
local_maximum_image_norm = np.zeros_like(local_maximum_image)
local_maximum_image_norm[img] = distance_image[img] / local_maximum_image[img]

#save
with h5py.File(file_path_output, 'w') as f:
    f.create_dataset('raw', data=local_maximum_image_norm, compression='gzip')
    f.create_dataset('distance_image', data = distance_image,compression='gzip')
    f.create_dataset('label', data=img, compression='gzip')
# SkelePlex

This folder contains the source code for training and using the SkelePlex skeletonziation network.

Note that the morphospaces library included here is for archival purposes. The maintained version of the library is: https://github.com/morphometrics/morphospaces

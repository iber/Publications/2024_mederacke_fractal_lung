from morphospaces.networks._components.unet_model import MultiscaleUnet3D
import numpy as np
import torch

net = MultiscaleUnet3D(in_channels=1, out_channels=2)

data = torch.from_numpy(np.ones((1, 1, 20, 20, 20))).float()
print(data.dtype)
print(data.shape)

output, decoder_outputs = net.training_forward(data)

print(output.shape)
print("decoder outputs:")
for decoder_output in decoder_outputs:
    print(decoder_output.shape)

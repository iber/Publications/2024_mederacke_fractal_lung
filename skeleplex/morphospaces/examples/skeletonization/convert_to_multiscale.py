import multiprocessing
from functools import partial
import glob
import os
from typing import Tuple

import h5py
import numpy as np
from skimage.measure import block_reduce
from scipy.ndimage import gaussian_filter, maximum_filter


def make_skeleton_target(skeleton_mask: np.ndarray, gaussian_sigma: float, normalization_neighborhood_size: int) -> np.ndarray:
    """
    Make a skeleton target from a skeleton mask.

    Parameters
    ----------
    skeleton_mask : np.ndarray
        A skeleton mask.
    gaussian_sigma : float
        The sigma for the gaussian blur.
    normalization_neighborhood_size : int
        The size of the neighborhood for computing the max for normalization.

    Returns
    -------
    skeleton_target : np.ndarray
        The prediction target for the skeleton.
    """
    # blur the target
    skeleton_target = gaussian_filter(skeleton_mask.astype(float), gaussian_sigma)

    # compute normalization values
    normalization_factors = maximum_filter(skeleton_target, size=normalization_neighborhood_size)

    # only normalize pixels where the local max is greater than 0
    normalization_mask = normalization_factors > 0
    values_to_normalize = skeleton_target[normalization_mask]
    masked_normalization_factors = normalization_factors[normalization_mask]
    skeleton_target[normalization_mask] = values_to_normalize / masked_normalization_factors

    return skeleton_target


def make_downscaled_ground_truth(
    label_image: np.ndarray,
    downscaling_factor: int = 2,
    gaussian_sigma: float = 1,
    normalization_neighborhood_size: int = 5,
) -> Tuple[np.ndarray, np.ndarray]:
    """
    Make a ground truth image from a label image.

    Parameters
    ----------
    label_image : np.ndarray
        The label image.
    downscaling_factor : int
        The factor by which to downscale the label image before making the ground truth.
    gaussian_sigma : float
        The sigma for the gaussian blur.
    normalization_neighborhood_size : int
        The size of the neighborhood for computing the max for normalization.

    Returns
    -------
    skeleton_target : np.ndarray
        The prediction target for the skeleton.
    reduced_labels : np.ndarray
        The downscaled label image.
    """
    # reduce the label image
    reduced_labels = block_reduce(label_image, block_size=downscaling_factor, func=np.max)

    skeleton_mask = reduced_labels == 2
    skeleton_target = make_skeleton_target(
        skeleton_mask=skeleton_mask,
        gaussian_sigma=gaussian_sigma,
        normalization_neighborhood_size=normalization_neighborhood_size,
    )

    return skeleton_target, reduced_labels


def convert_ground_truth(
        file_path: str,
        output_directory: str,
        gaussian_sigma: float = 1,
        normalization_neighborhood_size: int = 5,
) -> None:
    with h5py.File(file_path, "r") as f:
        normalized_vector_background_image = f["normalized_vector_background_image"][:]
        label_image = f["segmentation_label_image"][:]

    # make the scale 0 ground truth
    skeleton_mask = label_image == 2
    raw_skeletonization_target = make_skeleton_target(
        skeleton_mask=skeleton_mask,
        gaussian_sigma=gaussian_sigma,
        normalization_neighborhood_size=normalization_neighborhood_size,
    )

    # write the file
    file_name = os.path.basename(file_path)
    output_file_path = os.path.join(output_directory, file_name)
    with h5py.File(output_file_path, "w") as f_out:
        f_out.create_dataset(
            name="normalized_vector_background_image",
            data=normalized_vector_background_image,
            compression="gzip",
            chunks=(50, 50, 50)
        )

        # scale 0 ground truth
        f_out.create_dataset(
            name="label_image",
            data=label_image,
            compression="gzip",
            chunks=(50, 50, 50)
        )
        f_out.create_dataset(
            name="skeletonization_target",
            data=raw_skeletonization_target,
            compression="gzip",
            chunks=(50, 50, 50)
        )


if __name__ == "__main__":
    gaussian_sigma = 1
    normalization_neighborhood_size = 5

    # make the output directory
    output_directory = "/Users/kyamauch/Documents/automated_skeletonization/training_data/ground_truth"
    os.makedirs(output_directory, exist_ok=True)

    # get the files
    file_paths = glob.glob("/Users/kyamauch/Documents/automated_skeletonization/training_data/*.h5")

    # convert the files
    conversion_func = partial(
        convert_ground_truth,
        output_directory=output_directory,
        gaussian_sigma=gaussian_sigma,
        normalization_neighborhood_size=normalization_neighborhood_size,
    )
    with multiprocessing.get_context('spawn').Pool() as pool:
        pool.map(conversion_func, file_paths)
